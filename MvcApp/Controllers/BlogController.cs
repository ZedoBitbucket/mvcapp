﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Models;

namespace MvcApp.Controllers
{
	public class BlogController : Controller
	{
		private readonly BlogSet _ctx = new BlogSet();

		public ActionResult Index()
		{
			var model = _ctx.Blog.ToList();
				return View(model);
		}

		[HttpPost]
		public ActionResult Add(string tresc)
		{
			if (ModelState.IsValid)
			{
				_ctx.Blog.Add(new Blog() {DateTime = DateTime.Now, Tresc = tresc});
				_ctx.SaveChanges();
			}

			return RedirectToAction("Index");
		}

		public ActionResult Delete(int id)
		{
			var blog = _ctx.Blog.SingleOrDefault(s => s.Id == id);
			_ctx.Blog.Remove(blog);
			_ctx.SaveChanges();

			return RedirectToAction("Index");
		}
	}
}
