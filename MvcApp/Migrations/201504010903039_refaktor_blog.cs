namespace MvcApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class refaktor_blog : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Blog", "DateTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.Blog", "Tresc", c => c.String());
            DropColumn("dbo.Blog", "Content");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Blog", "Content", c => c.String(nullable: false, storeType: "xml"));
            DropColumn("dbo.Blog", "Tresc");
            DropColumn("dbo.Blog", "DateTime");
        }
    }
}
