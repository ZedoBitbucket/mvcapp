namespace MvcApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class set_add_xml_field : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Blog", "Content", c => c.String(nullable: false, storeType: "xml"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Blog", "Content", c => c.String(nullable: false));
        }
    }
}
