﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace MvcApplication1.Models
{
	public class Blog
	{
		[Key]
		public int Id { get; set; }
		public DateTime DateTime { get; set; }	
		public string Tresc { get; set; }

	}
}